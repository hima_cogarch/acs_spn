** Prerequisites **
Need Sigma installed from : https://bitbucket.org/sigma-development/sigma/wiki/Home
** Steps to run run the program **

1. Download latest Sigma from: https://bitbucket.org/sigma-development/sigma-release/wiki/Home

2. Download this repository

3. Compile the program after compiling Sigma

4. Run with (spn-len-3-sums-prod)

5. Program should print the posteriors