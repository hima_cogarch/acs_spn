(defparameter *non_lexical_productions_len3* '(
  ;(probability head X01 X13 X02 X23)                                               
  (0 * * * * *)
  (0.6 S A B * *) ; S -> A B 0.6
  (0.4 S B C * *) ; S -> B C 0.4
  (0.6 S * * A B)
  (0.4 S * * B C)
  (1 A  * * B A)
  (1 B * * C C)
  (1 C * * A B)
  (1 A B A * *)   ; A -> B A
  (1 B C C * *)  ;  B -> C C
  (1 C A B * *) ;  C -> A B                                           
  ))



(defun make_bottom_up_model nil

 (conditional 'observe-X01 
     :conditions '( 
        (word01 (word (word)))
       )
     :condacts '( 
        (X01 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )
  (conditional 'observe-X12 
     :conditions '( 
        (word12 (word (word)))
       )
     :condacts '( 
        (X12 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )
  (conditional 'observe-X23 
     :conditions '( 
        (word23 (word (word)))
       )
     :condacts '( 
        (X23 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )

;length 2 substrings
  (conditional 'compose-X02-top 
     :conditions '( 
        (X01 (head (left)))
        (X12 (head (right)))
       )
     :condacts '( 
        (X02 (head (head)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X13-top 
     :conditions '( 
        (X12 (head (left)))
        (X23 (head (right)))
       )
     :condacts '( 
        (X13 (head (head)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X03-top-0223
     :conditions '( 
        (X02 (head (left)))
        (X23 (head (right)))
       )
     :actions '( 
        (X03 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X03-top0113
     :conditions '( 
        (X01 (head (left)))
        (X13 (head (right)))
       )
     :actions '( 
        (X03 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

)

;;;;; NOW THE SPN MODEL 
(defun make_top_down_model nil

 (conditional 'compose-X23-bottom-13
     :conditions '(
        (X12 (head (left)))
        (X13-beta (head (head)))
        )
     :actions '(
        (X23-beta (head (right)))
        )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X23-bottom-03
     :conditions '(
        (X02 (head (left)))
        (X03-beta (head (head)))
        )
     :actions '(
        (X23-beta (head (right)))
        )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


 (conditional 'compose-X01-bottom-02
     :conditions '(
        (X12 (head (right)))
        (X02-beta (head (head)))
        )
     :actions '(
        (X01-beta (head (left)))
        )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X01-bottom-03
     :conditions '(
        (X13 (head (right)))
        (X03-beta (head (head)))
        )
     :actions '(
        (X01-beta (head (left)))
        )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X12-bottom-13
     :conditions '(
        (X23 (head (right)))
        (X13-beta (head (head)))
        )
     :actions '(
        (X12-beta (head (left)))
        )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X12-bottom-02
     :conditions '(
        (X02-beta (head (head)))
        (X01      (head (left)))
        )
     :actions '(
        (X12-beta (head (right)))
        )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X02-bottom
     :conditions '( 
        (X03-beta (head (head)) )
        (X23 (head (right)))

       )
     :actions '( 
        (X02-beta (head (left)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X13-bottom
     :conditions '( 
        (X03-beta (head (head)) )
        (X01 (head (left)))
       )
     :actions '( 
        (X13-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


#|
 (conditional 'compose-X23-bottom-13
     :conditions '(
        (X12 (head (left)))
        (X13 (head (head)))
        )
     :actions '(
        (X23 (head (right)))
        )
     :function *non_lexical_productions*
     :forward-conditional 'compose-X13-top
     :exclude-forward-backward t
  )
|#
)

(defparameter *combine_rules* '(
  (0 * * )                                
  (1 S S ) (1 A A) (1 B B) (1 C C ) 
  ))                                
  
(defun combine_them nil

  (conditional 'combine-X03
     :conditions '( 
        (X03 (head (head)))
        (X03-beta (head (head)))
       )
     :condacts '( 
        (X03-gamma (head (head)))
       )
 ;    :function *non_lexical_productions*
 ;    :function-variable-names '(head post)
  )


  (conditional 'combine-X02
     :conditions '( 
        (X02 (head (head)))
        (X02-beta (head (head)))
       )
     :condacts '( 
        (X02-gamma (head (head)))
       )
  )

  (conditional 'combine-X13
     :conditions '( 
        (X13 (head (head)))
        (X13-beta (head (head)))
       )
     :condacts '( 
        (X13-gamma (head (head)))
       )
  )

  (conditional 'combine-X01
     :conditions '( 
        (X01 (head (head)))
        (X01-beta (head (head)))
       )
     :condacts '( 
        (X01-gamma (head (head)))
       )
  )
  (conditional 'combine-X12
     :conditions '( 
        (X12 (head (head)))
        (X12-beta (head (head)))
       )
     :condacts '( 
        (X12-gamma (head (head)))
       )
  )

  (conditional 'combine-X23
     :conditions '( 
        (X23 (head (head)))
        (X23-beta (head (head)))
       )
     :condacts '( 
        (X23-gamma (head (head)))
       )
  )

  )


;grammar and stuff
(defparameter *lexicon* '(a b))
(defparameter *t_rule_heads* '(A B C))
(defparameter *nt_rule_heads* '(S A B C))

(defparameter *lexical_productions* '(
  (0 * *)
  (1 A a) (1 B b) (1 C a)
  ))

(defparameter *non_lexical_productions* '(
  (0 * * *)
  (0.6 S A B) ; S -> A B 0.6
  (0.4 S B C) ; S -> B C 0.4
  (1 A B A)   ; A -> B A
  (1 B C C)  ;  B -> C C
  (1 C A B) ;  C -> A B                                           
  ))

(defparameter *s_prod_SAB* '( (0.6 S A B)))
(defparameter *s_prod_SBC* '( (0.4 S B C)))
(defparameter *a_prod_ABA* '( (1 A B A)))
(defparameter *b_prod_BCC* '( (1 B C C)))
(defparameter *c_prod_CAB* '( (1 C A B)))


(defun make-bottom-up-model nil
;len 2 substrings

  (conditional 'compose-X02-top-S_BC
     :conditions '( 
        (X01-B (head (left)))
        (X12-C (head (right)))
       )
     :actions '( 
        (X02-S (head (head)) )
       )
     :function *s_prod_SBC*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X02-top-A_BA
     :conditions '( 
        (X01-B (head (left)))
        (X12-A (head (right)))
       )
     :actions '( 
        (X02-A (head (head)) )
       )
     :function *a_prod_aba*
     :function-variable-names '(head left right)
  )

 (conditional 'compose-X13-top-S_AB
     :conditions '( 
        (X12-A (head (left)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X13-S (head (head)) )
       )
     :function *s_prod_SAB*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X13-top-C_AB
     :conditions '( 
        (X12-A (head (left)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X02-C (head (head)) )
       )
     :function *c_prod_CAB*
     :function-variable-names '(head left right)
  )


; len 3 substrings
 (conditional 'compose-X03-top-S_AB_0223
     :conditions '( 
        (X02-A (head (left)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X03-S (head (head)) )
       )
     :function *s_prod_sab*
     :function-variable-names '(head left right)
  )


 (conditional 'compose-X03-top-S_BC_0113
     :conditions '( 
        (X01-B (head (left)))
        (X13-C (head (right)))
       )
     :actions '( 
        (X03-S (head (head)) )
       )
     :function *s_prod_sbc*
     :function-variable-names '(head left right)
  )

 (conditional 'compose-X03-top-C_AB_0223
     :conditions '( 
        (X02-A (head (left)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X03-C (head (head)) )
       )
     :function *c_prod_cab*
     :function-variable-names '(head left right)
  )


 )

(defun combine-them-all nil
  (conditional 'combine-X03-C
     :conditions '( 
        (X03-C (head (head)))
        (X03-C-beta (head (head)))
       )
     :condacts '( 
        (X03-C-gamma (head (head)))
       )
     )

  (conditional 'combine-X03-S
     :conditions '( 
        (X03-S (head (head)))
        (X03-S-beta (head (head)))
       )
     :condacts '( 
        (X03-S-gamma (head (head)))
       )
     )

  (conditional 'combine-X01-B
     :conditions '( 
        (X01-B (head (head)))
        (X01-B-beta (head (head)))
       )
     :condacts '( 
        (X01-B-gamma (head (head)))
       )
     )
  (conditional 'combine-X12-A
     :conditions '( 
        (X12-A (head (head)))
        (X12-A-beta (head (head)))
       )
     :condacts '( 
        (X12-A-gamma (head (head)))
       )
     )
  (conditional 'combine-X12-C
     :conditions '( 
        (X12-C (head (head)))
        (X12-C-beta (head (head)))
       )
     :condacts '( 
        (X12-C-gamma (head (head)))
       )
     )
  (conditional 'combine-X23-B
     :conditions '( 
        (X23-B (head (head)))
        (X23-B-beta (head (head)))
       )
     :condacts '( 
        (X23-B-gamma (head (head)))
       )
     )


#|
    (ppwm 'x02-A-gamma 'array)                 
    (ppwm 'x13-S-gamma 'array)                 
    (ppwm 'x13-C-gamma 'array)                 
|#

  (conditional 'combine-X02-A
     :conditions '( 
        (X02-A (head (head)))
        (X02-A-beta (head (head)))
       )
     :condacts '( 
        (X02-A-gamma (head (head)))
       )
     )
  (conditional 'combine-X13-C
     :conditions '( 
        (X13-C (head (head)))
        (X13-C-beta (head (head)))
       )
     :condacts '( 
        (X13-C-gamma (head (head)))
       )
     )

  (conditional 'combine-X13-S
     :conditions '( 
        (X13-S (head (head)))
        (X13-S-beta (head (head)))
       )
     :condacts '( 
        (X13-S-gamma (head (head)))
       )
     )

  )



(defun make-top-down-model nil

; len 2 substrings
#|
 (conditional 'compose-X12B-beta-A_BA_02_0112
     :conditions '( 
        (X02-A-beta (head (head)))
        (X01-B (head (left)))
       )
     :actions '( 
        (X12-A-beta (head (right)) )
       )
     :function *a_prod_aba*
     :function-variable-names '(head left right)
  )
|#
 (conditional 'compose-X12A-beta-A_BA_12_0112
     :conditions '( 
        (X02-A-beta (head (head)))
        (X01-B (head (left)))
       )
     :actions '( 
        (X12-A-beta (head (right)) )
       )
     :function *a_prod_aba*
     :function-variable-names '(head left right)
  )

 (conditional 'compose-X01B-beta-A_BA_02_0112
     :conditions '( 
        (X02-A-beta (head (head)))
        (X12-A (head (right)))
       )
     :actions '( 
        (X01-B-beta (head (left)) )
       )
     :function *a_prod_aba*
     :function-variable-names '(head left right)
  )
#| 
  (conditional 'compose-X01B-beta-A_BA_01_0112
     :conditions '( 
        (X02-A-beta (head (head)))
        (X12-A (head (right)))
       )
     :actions '( 
        (X01-B-beta (head (left)) )
       )
     :function *a_prod_aba*
     :function-variable-names '(head left right)
  )
|#

 (conditional 'compose-X12A-beta-C_AB_13_1223
     :conditions '( 
        (X13-C-beta (head (head)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X12-A-beta (head (left)) )
       )
     :function *c_prod_cab*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X23-B-beta-C_AB_13_1223
     :conditions '( 
        (X13-C-beta (head (head)))
        (X12-A (head (left)))
       )
     :actions '( 
        (X23-B-beta (head (right)) )
       )
     :function *c_prod_cab*
     :function-variable-names '(head left right)
  )






;len 3 substrings
 (conditional 'compose-X23B-beta-S_AB_0223
     :conditions '( 
        (X03-S-beta (head (head)))
        (X02-A (head (left)))
       )
     :actions '( 
        (X23-B-beta (head (right)) )
       )
     :function *s_prod_sab*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X02A-beta-S_AB_0223
     :conditions '( 
        (X03-S-beta (head (head)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X02-A-beta (head (left)) )
       )
     :function *s_prod_sab*
     :function-variable-names '(head left right)
  )


 (conditional 'compose-X02A-beta-C_AB_0223
     :conditions '( 
        (X03-C-beta (head (head)))
        (X23-B (head (right)))
       )
     :actions '( 
        (X02-A-beta (head (left)) )
       )
     :function *c_prod_cab*
     :function-variable-names '(head left right)
  )

 (conditional 'compose-X23B-beta-C_AB_0223
     :conditions '( 
        (X03-C-beta (head (head)))
        (X02-A (head (left)))
       )
     :actions '( 
        (X23-B-beta (head (right)) )
       )
     :function *c_prod_cab*
     :function-variable-names '(head left right)
  )


 (conditional 'compose-X13C-beta-S_BC_0113
     :conditions '( 
        (X03-S-beta (head (head)))
        (X01-B (head (left)))
       )
     :actions '( 
        (X13-C-beta (head (right)) )
       )
     :function *s_prod_sbc*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X01B-beta-S_BC_0113
     :conditions '( 
        (X03-S-beta (head (head)))
        (X13-C (head (right)))
       )
     :actions '( 
        (X01-B-beta (head (left)) )
       )
     :function *s_prod_sbc*
     :function-variable-names '(head left right)
  )

  )
; run_mode: bottom_up or bidirectional 

(defun spn-len-3-sums-prod nil

  (init)
  (new-type 'words          :constants  *lexicon* )
  (new-type 'terminal_heads    :constants *t_rule_heads*)
  (new-type 'nonterminal_heads   :constants *nt_rule_heads*)
  (new-type 'nt_head_s           :constants '(S) )
  (new-type 'nt_head_a           :constants '(A) )
  (new-type 'nt_head_b           :constants '(B) )
  (new-type 'nt_head_c           :constants '(C) )

  (new-type 'slot   :numeric t :discrete t :min 0 :max 2) ; usable range 1,2,3

  ;len 1 predicates
  (predicate 'X01-B  :world 'open :perception t :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X12-A  :world 'open :perception t :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X12-C  :world 'open :perception t :arguments '((head nt_head_c %)) :no-normalize t)
  (predicate 'X23-B  :world 'open :perception t :arguments '((head nt_head_b %)) :no-normalize t)


  (predicate 'X01-B-beta  :world 'open :perception t :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X12-A-beta  :world 'open :perception t :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X12-C-beta  :world 'open :perception t :arguments '((head nt_head_c %)) :no-normalize t)
  (predicate 'X23-B-beta  :world 'open :perception t :arguments '((head nt_head_b %)) :no-normalize t)

  (predicate 'X01-B-gamma  :world 'open :perception t :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X12-A-gamma  :world 'open :perception t :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X12-C-gamma  :world 'open :perception t :arguments '((head nt_head_c %)) :no-normalize t)
  (predicate 'X23-B-gamma  :world 'open :perception t :arguments '((head nt_head_b %)) :no-normalize t)

  ;len 2 predicates: not all are needed based on evidence
  (predicate 'X02-S  :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X02-A  :world 'open :learning-rate 0.0 :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X02-B  :world 'open :learning-rate 0.0 :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X02-C  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)

  (predicate 'X02-S-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X02-A-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X02-B-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X02-C-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)

  (predicate 'X02-S-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X02-A-gamma :world 'open :learning-rate 0.0 :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X02-B-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X02-C-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)

  (predicate 'X13-S  :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X13-A  :world 'open :learning-rate 0.0 :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X13-B  :world 'open :learning-rate 0.0 :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X13-C  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)
  
  (predicate 'X13-S-beta  :world 'open :perception t :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X13-A-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X13-B-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X13-C-beta  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)

  (predicate 'X13-S-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X13-A-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_a %)) :no-normalize t)
  (predicate 'X13-B-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_b %)) :no-normalize t)
  (predicate 'X13-C-gamma  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)


  ;len 3 predicates: only the ones ending in S are needed
  (predicate 'X03-S  :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X03-C  :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)
  (predicate 'X03-S-beta  :world 'open :perception t :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X03-C-beta  :world 'open :perception t :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)
  (predicate 'X03-S-gamma :world 'open :learning-rate 0.0 :arguments '((head nt_head_s %)) :no-normalize t)
  (predicate 'X03-C-gamma :world 'open :learning-rate 0.0 :arguments '((head nt_head_c %)) :no-normalize t)




  (predicate 'word01 :world 'open :perception t :arguments '((word words %) ))
  (predicate 'word12 :world 'open :perception t :arguments '((word words %) ))
  (predicate 'word23 :world 'open :perception t :arguments '((word words %) ))


  (make-bottom-up-model)
  (make-top-down-model)
  (combine-them-all)

  (format t "NOW PERCEIVING ~&~&~& ----> ~&~&~&")
  (perceive '(

    ; len 1 predicates             
    (X01-B (head B))
    (X12-A (head A))
    (X12-C (head C))
    (X23-B (head B))

    ; len 2 predicates
    (X13-S-beta 0 (head *) )
    (X12-C-beta 0 (head *) )
   ; (X12-A-beta 0 (head *) )

    ; len 3 predicates
    (X03-S-beta (head S))
    (X03-C-beta 0 (head *) )

   ; (word23 (word b))
   ; (X03-beta (head S))
    ))

  (setq post-d '(
     (format t "~&~&~&~& BOTTOM UP MESSAGES ~&~&~&")
     (format t "~&~&Length 1 predicates :~&~&")                 
     (ppwm 'x01-B 'array)                 
     (ppwm 'x12-A 'array)                 
     (ppwm 'x12-C 'array)         
        
     (ppwm 'x23-B 'array)                 
     (format t "~&~&Length 2 predicates :~&~&")                 
     (ppwm 'x02-S 'array)                 
     (ppwm 'x02-A 'array)  
               
     (ppwm 'x13-S 'array)
     (ppwm 'x13-C 'array)

     (format t "~&~&Length 3 predicates :~&~&")                 
     (ppwm 'x03-S 'array)                 
     (ppwm 'x03-C 'array)  


    (format t "~&~& True posteriors of all predicates :~&~&")                 
 
    (format t "~&~&Length 1 predicates :~&~&")                 
    (ppwm 'x01-B-gamma 'array)                 
    (ppwm 'x12-A-gamma 'array)                 
    (ppwm 'x12-C-gamma 'array)                 
    (ppwm 'x23-B-gamma 'array)                 
    
    (format t "~&~&Length 2 predicates :~&~&")                 
    (ppwm 'x02-A-gamma 'array)                 
    (ppwm 'x13-S-gamma 'array)                 
    (ppwm 'x13-C-gamma 'array)                 

    (format t "~&~&Length 3 predicates :~&~&")                 
 
     (ppwm 'x03-S-gamma 'array)                 
     (ppwm 'x03-C-gamma 'array)  

     ))  
  (d 1)
  )
