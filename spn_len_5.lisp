;;;; The model is validated both against the class slides (for bottom up messages only)
;;;;  and top down messages (using the pcfg em trainer class from github).
;;;; TODO: write a program to automatically generate this for : arbitrary sentences & arbitrary length of sentences

(defparameter *lexicon* '( with saw astronomers ears stars telescopes))
(defparameter *t_rule_heads* '(S PP VP P V NP))
(defparameter *nt_rule_heads* '(S PP VP P V NP))

(defparameter *lexical_productions* '(
  (0 * *)
;  (1 A a) (1 B b) (1 C a)
  (0.1 NP astronomers) (0.18 NP ears) (0.04 NP saw) (0.18 NP stars) (0.1 NP telescopes) (1 P with) (1 V saw)
  ))

(defparameter *non_lexical_productions* '(
  (0 * * *)
#|
  (0.6 S A B) ; S -> A B 0.6
  (0.4 S B C) ; S -> B C 0.4
  (1 A B A)   ; A -> B A
  (1 B C C)  ;  B -> C C
  (1 C A B) ;  C -> A B                                           
|#
  (1 S NP VP)
  (1 PP P NP)
  (0.7 VP V NP)
  (0.3 VP VP PP)
  (0.4 NP NP PP)
))


(defun make_bottom_up_model nil

 (conditional 'observe-X01 
     :conditions '( 
        (word01 (word (word)))
       )
     :condacts '( 
        (X01 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )
  (conditional 'observe-X12 
     :conditions '( 
        (word12 (word (word)))
       )
     :condacts '( 
        (X12 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )
  (conditional 'observe-X23 
     :conditions '( 
        (word23 (word (word)))
       )
     :condacts '( 
        (X23 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )

  (conditional 'observe-X34 
     :conditions '( 
        (word34 (word (word)))
       )
     :condacts '( 
        (X34 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )

  (conditional 'observe-X45 
     :conditions '( 
        (word45 (word (word)))
       )
     :condacts '( 
        (X45 (head (head)))
       )
     :function *lexical_productions*
     :function-variable-names '(head word)
  )


;length 2 substrings
  (conditional 'compose-X02-top 
     :conditions '( 
        (X01 (head (left)))
        (X12 (head (right)))
       )
     :condacts '( 
        (X02 (head (head)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X13-top 
     :conditions '( 
        (X12 (head (left)))
        (X23 (head (right)))
       )
     :condacts '( 
        (X13 (head (head)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X24-top 
     :conditions '( 
        (X23 (head (left)))
        (X34 (head (right)))
       )
     :condacts '( 
        (X24 (head (head)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X35-top 
     :conditions '( 
        (X34 (head (left)))
        (X45 (head (right)))
       )
     :condacts '( 
        (X35 (head (head)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )



;len 3 predicates: X03, X14, X25 

  (conditional 'compose-X03-top0113
     :conditions '( 
        (X01 (head (left)))
        (X13 (head (right)))
       )
     :actions '( 
        (X03 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X03-top-0223
     :conditions '( 
        (X02 (head (left)))
        (X23 (head (right)))
       )
     :actions '( 
        (X03 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X14-top1224 
     :conditions '( 
        (X12 (head (left)))
        (X24 (head (right)))
       )
     :actions '( 
        (X14 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X14-top-1334
     :conditions '( 
        (X13 (head (left)))
        (X34 (head (right)))
       )
     :actions '( 
        (X14 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X25-top2335
     :conditions '( 
        (X23 (head (left)))
        (X35 (head (right)))
       )
     :actions '( 
        (X25 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X25-top-2445
     :conditions '( 
        (X24 (head (left)))
        (X45 (head (right)))
       )
     :actions '( 
        (X25 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )




;len 4 predicates - X04 and X15
 (conditional 'compose-X04-top-0114
     :conditions '( 
        (X01 (head (left)))
        (X14 (head (right)))
       )
     :actions '( 
        (X04 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X04-top-0224
     :conditions '( 
        (X02 (head (left)))
        (X24 (head (right)))
       )
     :actions '( 
        (X04 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X04-top-0334
     :conditions '( 
        (X03 (head (left)))
        (X34 (head (right)))
       )
     :actions '( 
        (X04 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

 (conditional 'compose-X15-top-1225
     :conditions '( 
        (X12 (head (left)))
        (X25 (head (right)))
       )
     :actions '( 
        (X15 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X15-top-1335
     :conditions '( 
        (X13 (head (left)))
        (X35 (head (right)))
       )
     :actions '( 
        (X15 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X15-top-1445
     :conditions '( 
        (X14 (head (left)))
        (X45 (head (right)))
       )
     :actions '( 
        (X15 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


;len 5 predicates - thankfully theres only ONE : X05
 (conditional 'compose-X05-top-0115
     :conditions '( 
        (X01 (head (left)))
        (X15 (head (right)))
       )
     :actions '( 
        (X05 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
 (conditional 'compose-X05-top-0225
     :conditions '( 
        (X02 (head (left)))
        (X25 (head (right)))
       )
     :actions '( 
        (X05 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
    
 (conditional 'compose-X05-top-0335
     :conditions '( 
        (X03 (head (left)))
        (X35 (head (right)))
       )
     :actions '( 
        (X05 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
   
 (conditional 'compose-X05-top-0445
     :conditions '( 
        (X04 (head (left)))
        (X45 (head (right)))
       )
     :actions '( 
        (X05 (head (head)) )
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  
)

;;;;; NOW THE SPN MODEL 
(defun make_top_down_model nil

  ;No special acts of happening here
 
  ;len 4 predicates
  (conditional 'compose-X15-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X01 (head (left)))
       )
     :actions '( 
        (X15-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X04-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X45 (head (right)))
       )
     :actions '( 
        (X04-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  ;len 3 predicates
  (conditional 'compose-X25-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X02 (head (left)))
       )
     :actions '( 
        (X25-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X25-bottom-15
     :conditions '( 
        (X15-beta (head (head)) )
        (X12 (head (left)))
       )
     :actions '( 
        (X25-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X14-bottom-04
     :conditions '( 
        (X04-beta (head (head)) )
        (X01 (head (left)))
       )
     :actions '( 
        (X14-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X14-bottom-15
     :conditions '( 
        (X15-beta (head (head)) )
        (X45 (head (right)))
       )
     :actions '( 
        (X14-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X03-bottom-04
     :conditions '( 
        (X04-beta (head (head)) )
        (X34 (head (right)))
       )
     :actions '( 
        (X03-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X03-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X35 (head (right)))
       )
     :actions '( 
        (X03-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  ;len 2 predicates X02, X13, X24, X35
  (conditional 'compose-X02-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X25 (head (right)))
       )
     :actions '( 
        (X02-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X02-bottom-04
     :conditions '( 
        (X04-beta (head (head)) )
        (X24 (head (right)))
       )
     :actions '( 
        (X02-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X02-bottom-03
     :conditions '( 
        (X03-beta (head (head)) )
        (X23 (head (right)))
       )
     :actions '( 
        (X02-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
    
  (conditional 'compose-X13-bottom-03
     :conditions '( 
        (X03-beta (head (head)) )
        (X01 (head (left)))
       )
     :actions '( 
        (X13-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X13-bottom-14
     :conditions '( 
        (X14-beta (head (head)) )
        (X34 (head (right)))
       )
     :actions '( 
        (X13-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X13-bottom-15
     :conditions '( 
        (X15-beta (head (head)) )
        (X35 (head (right)))
       )
     :actions '( 
        (X13-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X24-bottom-04
     :conditions '( 
        (X04-beta (head (head)) )
        (X02 (head (left)))
       )
     :actions '( 
        (X24-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  
  (conditional 'compose-X24-bottom-14
     :conditions '( 
        (X14-beta (head (head)) )
        (X12 (head (left)))
       )
     :actions '( 
        (X24-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X24-bottom-25
     :conditions '( 
        (X25-beta (head (head)) )
        (X45 (head (right)))
       )
     :actions '( 
        (X24-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X35-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X03 (head (left)))
       )
     :actions '( 
        (X35-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X35-bottom-15
     :conditions '( 
        (X15-beta (head (head)) )
        (X13 (head (left)))
       )
     :actions '( 
        (X35-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  

  (conditional 'compose-X35-bottom-25
     :conditions '( 
        (X25-beta (head (head)) )
        (X23 (head (left)))
       )
     :actions '( 
        (X35-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )



  ;len 1 predicates: X01, X12, X23, X34, X45
  (conditional 'compose-X01-bottom-02
     :conditions '( 
        (X02-beta (head (head)) )
        (X12 (head (right)))
       )
     :actions '( 
        (X01-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X01-bottom-03
     :conditions '( 
        (X03-beta (head (head)) )
        (X13 (head (right)))
       )
     :actions '( 
        (X01-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X01-bottom-04
     :conditions '( 
        (X04-beta (head (head)) )
        (X14 (head (right)))
       )
     :actions '( 
        (X01-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X01-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X15 (head (right)))
       )
     :actions '( 
        (X01-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X12-bottom-02
     :conditions '( 
        (X02-beta (head (head)) )
        (X01 (head (left)))
       )
     :actions '( 
        (X12-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X12-bottom-13
     :conditions '( 
        (X13-beta (head (head)) )
        (X23 (head (right)))
       )
     :actions '( 
        (X12-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X12-bottom-14
     :conditions '( 
        (X14-beta (head (head)) )
        (X24 (head (right)))
       )
     :actions '( 
        (X12-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X12-bottom-15
     :conditions '( 
        (X15-beta (head (head)) )
        (X25 (head (right)))
       )
     :actions '( 
        (X12-beta (head (left)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X23-bottom-03
     :conditions '( 
        (X03-beta (head (head)) )
        (X02 (head (left)))
       )
     :actions '( 
        (X23-beta (head (right)))

       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )

  (conditional 'compose-X23-bottom-13
     :conditions '( 
        (X13-beta (head (head)) )
        (X12 (head (left)))
       )
     :actions '( 
        (X23-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X23-bottom-24
     :conditions '( 
        (X24-beta (head (head)) )
        (X34 (head (right)))
       )
     :actions '( 
        (X23-beta (head (left)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X23-bottom-25
     :conditions '( 
        (X25-beta (head (head)) )
        (X35 (head (right)))
       )
     :actions '( 
        (X23-beta (head (left)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X34-bottom-04
     :conditions '( 
        (X04-beta (head (head)) )
        (X03 (head (left)))
       )
     :actions '( 
        (X34-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X34-bottom-14
     :conditions '( 
        (X14-beta (head (head)) )
        (X13 (head (left)))
       )
     :actions '( 
        (X34-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X34-bottom-24
     :conditions '( 
        (X24-beta (head (head)) )
        (X23 (head (left)))
       )
     :actions '( 
        (X34-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X34-bottom-35
     :conditions '( 
        (X35-beta (head (head)) )
        (X45 (head (right)))
       )
     :actions '( 
        (X34-beta (head (left)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X45-bottom-05
     :conditions '( 
        (X05-beta (head (head)) )
        (X04 (head (left)))
       )
     :actions '( 
        (X45-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
  (conditional 'compose-X45-bottom-15
     :conditions '( 
        (X15-beta (head (head)) )
        (X14 (head (left)))
       )
     :actions '( 
        (X45-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X45-bottom-25
     :conditions '( 
        (X25-beta (head (head)) )
        (X24 (head (left)))
       )
     :actions '( 
        (X45-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )


  (conditional 'compose-X45-bottom-35
     :conditions '( 
        (X35-beta (head (head)) )
        (X34 (head (left)))
       )
     :actions '( 
        (X45-beta (head (right)))
       )
     :function *non_lexical_productions*
     :function-variable-names '(head left right)
  )
)
                              
(defun combine_them nil


  ;length 5 predicates
  (conditional 'combine-X05
     :conditions '( 
        (X05 (head (head)))
        (X05-beta (head (head)))
       )
     :condacts '( 
        (X05-gamma (head (head)))
       )
  )
  ;length 4 predicates

  (conditional 'combine-X04
     :conditions '( 
        (X04 (head (head)))
        (X04-beta (head (head)))
       )
     :condacts '( 
        (X04-gamma (head (head)))
       )
  )
  (conditional 'combine-X15
     :conditions '( 
        (X15 (head (head)))
        (X15-beta (head (head)))
       )
     :condacts '( 
        (X15-gamma (head (head)))
       )
  )
  ;length 3 predicates
  (conditional 'combine-X25
     :conditions '( 
        (X25 (head (head)))
        (X25-beta (head (head)))
       )
     :condacts '( 
        (X25-gamma (head (head)))
       )
  )
  (conditional 'combine-X14
     :conditions '( 
        (X14 (head (head)))
        (X14-beta (head (head)))
       )
     :condacts '( 
        (X14-gamma (head (head)))
       )
  )
  (conditional 'combine-X03
     :conditions '( 
        (X03 (head (head)))
        (X03-beta (head (head)))
       )
     :condacts '( 
        (X03-gamma (head (head)))
       )
  )

  ;length 2 predicates 

  (conditional 'combine-X02
     :conditions '( 
        (X02 (head (head)))
        (X02-beta (head (head)))
       )
     :condacts '( 
        (X02-gamma (head (head)))
       )
  )

  (conditional 'combine-X13
     :conditions '( 
        (X13 (head (head)))
        (X13-beta (head (head)))
       )
     :condacts '( 
        (X13-gamma (head (head)))
       )
  )

  (conditional 'combine-X24
     :conditions '( 
        (X24 (head (head)))
        (X24-beta (head (head)))
       )
     :condacts '( 
        (X24-gamma (head (head)))
       )
  )

  (conditional 'combine-X35
     :conditions '( 
        (X35 (head (head)))
        (X35-beta (head (head)))
       )
     :condacts '( 
        (X35-gamma (head (head)))
       )
  )

  ;length 1 predicates
  (conditional 'combine-X01
     :conditions '( 
        (X01 (head (head)))
        (X01-beta (head (head)))
       )
     :condacts '( 
        (X01-gamma (head (head)))
       )
  )
  (conditional 'combine-X12
     :conditions '( 
        (X12 (head (head)))
        (X12-beta (head (head)))
       )
     :condacts '( 
        (X12-gamma (head (head)))
       )
  )

  (conditional 'combine-X23
     :conditions '( 
        (X23 (head (head)))
        (X23-beta (head (head)))
       )
     :condacts '( 
        (X23-gamma (head (head)))
       )
  )

  (conditional 'combine-X34
     :conditions '( 
        (X34 (head (head)))
        (X34-beta (head (head)))
       )
     :condacts '( 
        (X34-gamma (head (head)))
       )
  )

  (conditional 'combine-X45
     :conditions '( 
        (X45 (head (head)))
        (X45-beta (head (head)))
       )
     :condacts '( 
        (X45-gamma (head (head)))
       )
  )


  )
; run_mode: bottom_up or bidirectional 
(defun spn-len-5 nil

  (init)
  (new-type 'words          :constants  *lexicon* )
  (new-type 'terminal_heads    :constants *t_rule_heads*)
  (new-type 'nonterminal_heads   :constants *nt_rule_heads*)
  (new-type 'slot   :numeric t :discrete t :min 0 :max 2) ; usable range 1,2,3

; length 1 predicates
  (predicate 'X01 :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X12 :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X23 :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X34 :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X45 :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
; length 2 predicates
  (predicate 'X02 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X13 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X24 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X35 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

;length 3 predicates ;;(slot slot %)
  (predicate 'X03 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X14 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X25 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

;length 4 predicates 
  (predicate 'X04 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X15 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

;length 5 predicates 
  (predicate 'X05 :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

;;now top down predicates (also called beta )
; length 1 predicates
  (predicate 'X01-beta :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X12-beta :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X23-beta :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X34-beta :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X45-beta :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
; length 2 predicates
  (predicate 'X02-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X13-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X24-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X35-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
;length 3 predicates ;;(slot slot %)
  (predicate 'X03-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X14-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X25-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
;length 4 predicates ;; (slot slot %)
  (predicate 'X04-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X15-beta :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
;length 5 predicates ;;
  (predicate 'X05-beta :world 'open :perception t :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) )

;;; now the posteriors (called gamma in literature)
; length 1 predicates
  (predicate 'X01-gamma :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X12-gamma :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X23-gamma :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X34-gamma :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
  (predicate 'X45-gamma :world 'open :learning-rate 0.0  :arguments '( (head terminal_heads %) ) :no-normalize t)
; length 2 predicates
  (predicate 'X02-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X13-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X24-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X35-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

;length 3 predicates ;;(slot slot %)
  (predicate 'X03-gamma :world 'open :perception t :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X14-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X25-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

;length 4 predicates 
  (predicate 'X04-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
  (predicate 'X15-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)
;length 5 predicates
  (predicate 'X05-gamma :world 'open :learning-rate 0.0  :arguments '( (head nonterminal_heads %) ) :no-normalize t)

  (predicate 'word01 :world 'open :perception t :arguments '((word words %) ))
  (predicate 'word12 :world 'open :perception t :arguments '((word words %) ))
  (predicate 'word23 :world 'open :perception t :arguments '((word words %) ))
  (predicate 'word34 :world 'open :perception t :arguments '((word words %) ))
  (predicate 'word45 :world 'open :perception t :arguments '((word words %) ))


  (make_bottom_up_model)
  (make_top_down_model)
  (combine_them)
 
  (format t "NOW PERCEIVING ~&~&~& ----> ~&~&~&")
  (perceive '(
    (word01 (word astronomers))
    (word12 (word saw))
    (word23 (word stars))
    (word34 (word with))
    (word45 (word ears))

    (X05-beta (head S))
    ))

  (setq post-d '(
     (format t "~&~&~&~& BOTTOM UP MESSAGES ~&~&~&")
     (format t "~&~&Length 1 predicates :~&~&")                 
     (ppwm 'x01 'array)                 
     (ppwm 'x12 'array)                 
     (ppwm 'x23 'array)                 
     (ppwm 'x34 'array)                 
     (ppwm 'x45 'array)
     (format t "~&~&Length 2 predicates :~&~&")                 
     (ppwm 'x02 'array)                 
     (ppwm 'x13 'array)
     (ppwm 'x24 'array)
     (ppwm 'x35 'array)
     (format t "~&~&Length 3 predicates :~&~&")                 
     (ppwm 'x03 'array)  
     (ppwm 'x14 'array)
     (ppwm 'x25 'array)
     (format t "~&~&Length 4 predicates :~&~&")                 
     (ppwm 'x04 'array)  
     (ppwm 'x15 'array)  

     (format t "~&~&Length 5 predicates :~&~&")                 
     (ppwm 'x05 'array)  
     
     (format t "~&~&~&~& TOP DOWN MESSAGES: ~&~&~&")

     (format t "~&~&Length 5 predicates :~&~&")                 
     (ppwm 'X05-beta 'array)  

     (format t "~&~&Length 4 predicates :~&~&")                 
     (ppwm 'X04-beta 'array)
     (ppwm 'X15-beta 'array)

     (format t "~&~&Length 3 predicates :~&~&")                 
     (ppwm 'x03-beta 'array)
     (ppwm 'x14-beta 'array)
     (ppwm 'x25-beta 'array)

     (format t "~&~&Length 2 predicates :~&~&")                 
     (ppwm 'x02-beta 'array)
     (ppwm 'x13-beta 'array)
     (ppwm 'x24-beta 'array)
     (ppwm 'x35-beta 'array)


     (format t "~&~&Length 1 predicates :~&~&")                 
     (ppwm 'x01-beta 'array)
     (ppwm 'x12-beta 'array)
     (ppwm 'x23-beta 'array)
     (ppwm 'x34-beta 'array)
     (ppwm 'x45-beta 'array)


     (format t "~&~&~&~& POSTERIORS ~&~&~&")
     (format t "~&~&Length 5 predicates :~&~&")                 
     (ppwm 'x05-gamma 'array)  

     (format t "~&~&Length 4 predicates :~&~&")                 
     (ppwm 'x04-gamma 'array)
     (ppwm 'x15-gamma 'array)

    (format t "~&~&Length 3 predicates :~&~&")                 
     (ppwm 'x03-gamma 'array)
     (ppwm 'x14-gamma 'array)
     (ppwm 'x25-gamma 'array)


     (format t "~&~&Length 2 predicates :~&~&")                 
     (ppwm 'x02-gamma 'array)
     (ppwm 'x13-gamma 'array)
     (ppwm 'x24-gamma 'array)
     (ppwm 'x35-gamma 'array)

     (format t "~&~&Length 1 predicates :~&~&")                 
     (ppwm 'x01-gamma 'array)
     (ppwm 'x12-gamma 'array)
     (ppwm 'x23-gamma 'array)
     (ppwm 'x34-gamma 'array)
     (ppwm 'x45-gamma 'array)
 


   ))  
  (d 1)
  )
